package com.example.dubbo.provider.service.impl;

import com.example.dubbo.dto.req.DemoReq;
import com.example.dubbo.dto.res.DemoRes;
import com.example.dubbo.service.DemoService;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @author chenhui_sinosoft
 * @version 1.0.0
 */
@DubboService
public class DemoServiceImpl implements DemoService {

    @Override
    public DemoRes demo(DemoReq demoReq) {
        int i = 0;
        int a=0/i;
        DemoRes demoRes = new DemoRes();
        demoRes.setParm1("返回");
        demoRes.setParm2(2);
        return demoRes;
    }
}
