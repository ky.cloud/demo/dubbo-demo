package com.example.dubbo.service;

import com.example.dubbo.dto.req.DemoReq;
import com.example.dubbo.dto.res.DemoRes;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public interface DemoService {

    DemoRes demo(DemoReq demoReq);

}
