package com.example.dubbo.dto.res;

import com.ky.cloud.components.dubbo.dto.BaseResultDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class DemoRes extends BaseResultDTO {

    private String parm1;

    private Integer parm2;
}
