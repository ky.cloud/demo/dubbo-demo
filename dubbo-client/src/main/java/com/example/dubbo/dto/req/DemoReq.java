package com.example.dubbo.dto.req;

import lombok.Data;

import java.io.Serializable;

@Data
public class DemoReq implements Serializable {

    private String parm1;

    private Integer parm2;
}
