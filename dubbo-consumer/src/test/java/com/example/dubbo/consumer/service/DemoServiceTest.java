package com.example.dubbo.consumer.service;

import com.example.dubbo.dto.req.DemoReq;
import com.example.dubbo.dto.res.DemoRes;
import com.example.dubbo.service.DemoService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoServiceTest {
    @DubboReference
    private DemoService demoService;
    @Test
    void demo() {
        DemoReq demoReq = new DemoReq();
        demoReq.setParm1("测试");
        demoReq.setParm2(1);
        DemoRes demo = demoService.demo(demoReq);
    }
}