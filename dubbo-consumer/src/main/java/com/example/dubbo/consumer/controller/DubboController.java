package com.example.dubbo.consumer.controller;

import com.example.dubbo.dto.req.DemoReq;
import com.example.dubbo.dto.res.DemoRes;
import com.example.dubbo.service.DemoService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chenhui_sinosoft
 * @version 1.0.0
 */
@RestController
public class DubboController {

    @DubboReference
    private DemoService demoService;

    @GetMapping("/demo")
    public void test(){
        DemoReq demoReq = new DemoReq();
        demoReq.setParm1("测试");
        demoReq.setParm2(1);
        DemoRes demo = demoService.demo(demoReq);
    }
}
